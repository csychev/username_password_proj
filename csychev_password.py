from errno import EAGAIN
import time
username_passwords = {}
users = []


option = 0
#The main function will present the menu and give the user the 3 options
#Also catches any value error entered by the user

print("""Welcome to the Username and Password Storage Program\n""", end="")
def main():
    
    
    correct_option= False
    #While loop will run until the user enters an option 
    while(correct_option == False):
        try:
            option = int(input("""Please enter an option:
              
1. Create a Username and Password
2. Log in 
3. Exit \n"""))
            
            if (option == 1) or (option == 2) or (option == 3):
                correct_option = True
            else:
                print("Please enter one of the options listed\n")
        
              
        except ValueError:
            print("Please enter one of the options listed\n")
    
    #Call the check input function once the loop is broken    
    check_input(option)
#This function takes the user input from the menu and decides what to do with it
def check_input(option):
     
        #create new username and pw
        if option == 1:
            
            u_name1 = input("Enter your school email for your username (spaces will be stripped) ::\n")
            u_name1_strip = u_name1.replace(" ", "").strip()
            #first make sure it's a valid username
            u = check_requirements_U(u_name1_strip, 1)
            u_pass1 = input("Enter a password between 6-10 characters and at least one number (spaces will be stripped) ::\n")
            #make sure it's a valid password
            u_pass1_strip = u_pass1.replace(" ", "").strip()
            p = check_requirements_P(u_pass1_strip, 1)
            exist = check_existing(u_name1_strip, u_pass1_strip, 1)
              
            if u and p:
                #make sure the username/password doesn't already exist in the text file
                if exist == True:
                    print("\n!!!!Your username/password already exists, please try again!!!!\n")
                    #if so return to the first option
                
                    check_input(1)
                #Open the username and password file
                try:
                    #opens a text file and adds usernames and passwords into them
                    username_passwords.update({u_name1_strip:u_pass1_strip})
                    txt_User_Pass = ', '.join("{!s}={!r}".format(key,val) for (key,val) in username_passwords.items())
                    open_file = open("usernames_and_password.txt", "a+")
                    open_file.seek(0)
                    open_file.write("\n")
                    open_file.write(txt_User_Pass)
                    
                    open_file.close()
                    print("""
 _____________________________________________________
                                                         
 Success! Your username and password has been created!
_____________________________________________________\n""")
                    time.sleep(1)
                    print("______________________________")
                    print("We'll now return to the menu.")
                except IOError as e:
                    print(e)

                time.sleep(2)
                main() 
            else:
                print("Either your username or password doesn't meet the requirements, try again")
                check_input(1)
            
            
                      

        if option == 2:
            #Checks for exsiting
            username_option_2 = input("Please enter your school email\n")
            u = check_requirements_U(username_option_2, 2)
            password_option_2 = input("Please enter your password\n")
            p = check_requirements_P(password_option_2, 2)
            if u and p:
                b = check_existing(username_option_2, password_option_2, 2)
                if b:
                    print("Login successful! We will now return to the menu.\n")
                    time.sleep(1.5)
                    main()
                    
                else:
                    UP_does_exist = False
                    print("""Your username or password does not exist, would you like to try again or create a new username and password?
    1. Try again
    2. Create username and password""")
                    while(UP_does_exist == False):
                        
                        try:
                            try_again = int(input(''))
                            if try_again == 1:
                                check_input(2)
                                UP_does_exist = True
                            elif try_again == 2:
                                check_input(1)
                                UP_does_exist = True
                            else:
                                print("Please enter one of the options listed\n")
                            continue
                        except ValueError:
                            print("Please enter one of the options listed\n")
                        

                        
                        
                        
                    check_input(2)  
            else:
                print("Incorrect username and password, try again\n")
                check_input(2)
                 
        if option == 3:
            exit()
             
        
                    
   
#returns a boolean, regarding whether or not a username/password exists     
def check_existing(username, password, opt):
    try:
        does_exist = False
        read_file = open("usernames_and_password.txt", "r+")
        read_file.seek(0)
        getLines = read_file.readlines()
        for line in getLines:
            equal_sign_Index = line.index("=")
            string_no_quotes = line.replace("'","")
            infile_username = string_no_quotes[0:equal_sign_Index].strip()
            infile_password = string_no_quotes[equal_sign_Index+1:line.__len__()+1].strip()
            if opt == 2:
                if username == infile_username and password == infile_password:
                    does_exist = True
            elif opt == 1:
                if username == infile_username or password == infile_password:
                    does_exist = True
                else:
                    does_exist = False
       
        return does_exist
    except IOError as e:
        return e
                     
#Returns a list of all the indces of the periods in a string    
def findDots(s):
    return [i for i, letter in enumerate(s) if letter == "."]
#Checks for the validity of the email, using a system of passes     
def check_requirements_U(username, opt):
    #check for:no two periods in a row, the characters between @ and .edu are none zero
    
    passes = 0
    the_at = -1
    the_at_count = 0
    dots = findDots(username)
    #find that there is an @ sign
    for character in range(username.__len__()):
        if username[character] == "@":
            the_at = character
            passes += 1
    #count the instances of @
    for c in username:
        if c == "@":
            the_at_count+=1
    #If there is only one it gets a pass       
    if the_at_count == 1:
        passes+=1
    else:
        print("Not a valid email address, can't have more than one @ sign, try again\n")
        check_input(opt)
    #if these is no @ at all then it isn't valid             
    if the_at == -1:
        print("Not a valid email address, you need an @ sign, try again\n")
        check_input(opt)
        
    #if there are two periods in a row then it is also not valid               
    for index in dots:
        if index == index + 1:
            print("Not a valid email address, you can't have more than two periods in a row, try again\n")
            check_input(opt)
             
    #check that the last 4 characters are .edu    
    if username[-4:] == ".edu":
        passes += 1
         
    else: 
        print("Not a valid email address, needs to be a part of the .edu domain try again\n")
        check_input(opt)
     
    #make sure that there is something between the @ and the .edu
    if username[the_at+1] == ".":
        print("Not a valid email address, try again\n")
        check_input(opt)
    #check that there is something before the @  
    if username[0:the_at] == "":
        print("Not a valid email address, needs something before the @ try again\n")
        check_input(opt)
                 
    if passes == 3:
        all_passes = True
    
    else:
        all_passes = False
        
        
         
    return all_passes
 
#Checks if the requirements of the password are met 
#any time a condition is not met the check_input function will be called again         
def check_requirements_P(password, opt):
    passes = 0
    num_count = 0
    #Checks length 
    if 5 < password.__len__() < 11:
        passes += 1
        
    else:
        print("Not a valid password, try again\n")
        check_input(opt)
    #checks for a number in the password
    for i in password:
        if i.isdigit():
            num_count += 1
    #checks that there's at least one number
    if num_count>0:
        passes += 1
    else:
        print("Not a valid password, try again\n")
        check_input(opt)
          
    if passes == 2:
        all_passes = True
       
    else:
        all_passes = False
      
         
    return all_passes
     
main()
