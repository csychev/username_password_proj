import pickle

def main(): 
    userInput = 0
    isTrue = False
    while (isTrue):
        print('''
    1)Create username and password
    2) Log in
    3) Exit
    ''')
        userInput = input ("Enter your choice: ")
        if userInput == 1:
            userPass()
        elif userInput == 2:
            login()
        elif userInput == 3:
            print ("You’ve successfully exited the program")
            isTrue = True
        else:
            print ("Please choose options 1 - 3.")
        
def loaddict():
    try:
        binFileobj = open("dictAcc.dat", "rb")
        return pickle.load
    except IOError as e:
        print(e)
        

def savedict(dictAcc):
    binFileobj = open("dictAcc.dat", "rb")
    pickle.dump(dictAcc, binFileobj) 
 
def userPass():
    dictAcc = loaddict()
    userName = input ("Please Set Your New Username: ")
    password = input ("Please Set Your New Password: ")
    if (userName in dictAcc):
        print("Username is taken")
        userPass()
    elif userName not in dictAcc :
        print("Congratulations! You have successfully created an account!")
        savedict(dictAcc) 
    else:
        print("Invalid answer, try again")
        userPass()
            
def login():
    global userName
    global password
    global tries
    loginUserName = input ("Type in your Username: ")
    loginPass = input ("Type in your Password: ")
    dictAcc = loaddict() 
    if (tries < 3):
        for key in dictAcc:
            if (loginUserName == key and loginPass == dictAcc[key]):
                print("You have successfully logged in!")
            else:
                print("Please try again")
                tries += 1
                login()
            if (tries >= 3):
                print("You have attempted to login too many times. Try again later.")
                tries=1
                login()

global tries
tries=1
userPass()
